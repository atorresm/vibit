extends Node2D

onready var sprite = get_node("Sprite")
onready var anim = get_node("AnimationPlayer")
var current_color = "empty"

var orange_texture = preload("res://Assets/point_orange.png")
var yellow_texture = preload("res://Assets/point_yellow.png")

func _ready():
	pass

func _change_color(color):
	if color == "orange":
		sprite.set_texture(orange_texture)
		self.current_color = "orange"
	elif color == "yellow":
		sprite.set_texture(yellow_texture)
		self.current_color = "yellow"

func get_current_color():
	return self.current_color

func on_round_end(color):
	_change_color(color)
	anim.play("round_ended") 
	current_color = color

# this func is called when the animation for the round end ends (see AnimationPlayer)
func _reset_after_anim():
	sprite.set_opacity(1)
	sprite.set_scale(Vector2(1,1))