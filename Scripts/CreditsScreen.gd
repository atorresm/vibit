extends Node2D

onready var anim = get_node("AnimationPlayer")
onready var touch = get_node("TouchDetector")

func _ready():
	pass


func _on_TouchDetector_pressed():
	anim.play("fade_out")

# this function is called when the fadeout ends
func _change_to_mainmenu():
		get_node("/root/global").goto_scene("res://Scenes/MainMenu.tscn")
	
