extends Node2D

onready var anim = get_parent().get_node("AnimationPlayer")
var _screen = ""

func _ready():
	anim.play("fade_in")

func _on_play_pressed():
	anim.play("fade_out")
	var rounds_selected = int(get_node("NumberSelector/number").get_text())
	Globals.set("rounds", rounds_selected)
	self._screen = "play"

func _on_help_pressed():
	anim.play("fade_out")
	self._screen = "help"

# this function runs at the end of the animation
func _select_next_screen():
	if self._screen == "play":
		get_node("/root/global").goto_scene("res://Scenes/GameScreen.tscn")
	elif self._screen == "help":
		get_node("/root/global").goto_scene("res://Scenes/CreditsScreen.tscn")
