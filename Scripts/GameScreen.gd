extends Node2D

onready var points = get_node("Points").get_children()
onready var red_player_button = get_node("PlayerButtons/OrangePlayer")
onready var yellow_player_button = get_node("PlayerButtons/YellowPlayer")
onready var anim = get_node("Points/AnimationPlayer")
var _current_round_pointer
var rounds = 0
var selected_rounds = Globals.get("rounds")
var _rounds_won_by_orange = 0
var _rounds_won_by_yellow = 0
var _is_vibrating = false
var _elapsed_seconds = 0
var vibrator = Globals.get_singleton("GodotVibrate")
var touched = false
var _touched_orange = false
var _touched_yellow = false
var _random_offset = 0

func _ready():
	if selected_rounds == 1:
		_current_round_pointer = 3 # 4th pointer
	elif selected_rounds == 3:
		_current_round_pointer = 2 # 3rd pointer
	elif selected_rounds == 5:
		_current_round_pointer = 1 # 2nd pointer
	elif selected_rounds == 7:
		_current_round_pointer = 0 # 1st pointer
	_randomize_offset()
	set_process(true)

func _process(delta):
	_elapsed_seconds += delta
	if (_elapsed_seconds > (2 + _random_offset)):
		vibrator.vibrate(20)
		if touched:
			if _touched_orange:
				_end_round("orange")
			elif _touched_yellow:
				_end_round("yellow")
	else:
		_touched_orange = false
		_touched_yellow = false

func _end_round(color):
	touched = false
	_touched_orange = false
	_touched_yellow = false
	_elapsed_seconds = 0
	_randomize_offset()
	_go_to_next_round(color)
	

func _go_to_next_round(winnercolor):
	if (winnercolor == "orange"):
		_rounds_won_by_orange += 1
	elif (winnercolor == "yellow"):
		_rounds_won_by_yellow += 1
	
	points[_current_round_pointer].on_round_end(winnercolor)
	rounds += 1
	
	if rounds < selected_rounds:
		_current_round_pointer += 1
	else:
		print("end")
		_endgame()
	# Print statements for debugging
	#print("Orange wins: " + str(_rounds_won_by_orange))
	#print("Yellow wins: " + str(_rounds_won_by_yellow))
	#print("Last round: " + str(rounds))
	

func _randomize_offset():
	randomize() # randomize seed
	_random_offset = rand_range(0, 3)

func _endgame():
	if _rounds_won_by_orange > _rounds_won_by_yellow:
		Globals.set("winner", "orange")
	else:
		Globals.set("winner", "yellow")
	anim.play("fade_out")

# this func is called after the fade_out animation
func _go_to_game_over_screen():
			get_node("/root/global").goto_scene("res://Scenes/GameOverScreen.tscn")

func _on_OrangePlayer_pressed():
	touched = true
	_touched_orange = true

func _on_YellowPlayer_pressed():
	touched = true
	_touched_yellow = true