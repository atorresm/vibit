extends Node2D

onready var yellowlabel = get_node("UI/ResultsLabels/RedZoneLabel") # these two are swapped for rotation
onready var redlabel = get_node("UI/ResultsLabels/YellowZoneLabel") 
onready var anim = get_node("UI/ResultsLabels/AnimationPlayer")
var winner = ""
var _next_screen = ""

func _ready():
	winner = Globals.get("winner")
	if winner == "orange":
		redlabel.set_text("Winner")
		yellowlabel.set_text("Loser")
	elif winner == "yellow":
		redlabel.set_text("Loser")
		yellowlabel.set_text("Winner")

func _on_arrow_back_pressed():
	self._next_screen = "mainmenu"
	anim.play("fade_out")

func _on_replay_pressed():
	self._next_screen = "replay"
	anim.play("fade_out")

func _go_to_next_screen():
	if self._next_screen == "mainmenu":
		get_node("/root/global").goto_scene("res://Scenes/MainMenu.tscn")
	elif self._next_screen == "replay":
		get_node("/root/global").goto_scene("res://Scenes/GameScreen.tscn")
