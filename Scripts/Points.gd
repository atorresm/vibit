extends Node2D

onready var point_1 = get_node("Point1")
onready var point_2 = get_node("Point2")
onready var point_3 = get_node("Point3")
onready var point_4 = get_node("Point4")
onready var point_5 = get_node("Point5")
onready var point_6 = get_node("Point6")
onready var point_7 = get_node("Point7")

onready var anim = get_node("AnimationPlayer")


func _ready():
	# play the fade-in animation
	anim.play("fade_in")
	# sorry for the ugly code
	var rounds_selected = Globals.get("rounds")
	if rounds_selected == 1:
		point_1.set_opacity(0)
		point_2.set_opacity(0)
		point_3.set_opacity(0)
		point_4.set_opacity(1)
		point_5.set_opacity(0)
		point_6.set_opacity(0)
		point_7.set_opacity(0)
	elif rounds_selected == 3:
		point_1.set_opacity(0)
		point_2.set_opacity(0)
		point_3.set_opacity(1)
		point_4.set_opacity(1)
		point_5.set_opacity(1)
		point_6.set_opacity(0)
		point_7.set_opacity(0)
	elif rounds_selected == 5:
		point_1.set_opacity(0)
		point_2.set_opacity(1)
		point_3.set_opacity(1)
		point_4.set_opacity(1)
		point_5.set_opacity(1)
		point_6.set_opacity(1)
		point_7.set_opacity(0)
	elif rounds_selected == 7:
		point_1.set_opacity(1)
		point_2.set_opacity(1)
		point_3.set_opacity(1)
		point_4.set_opacity(1)
		point_5.set_opacity(1)
		point_6.set_opacity(1)
		point_7.set_opacity(1)
