extends Node2D

onready var number = get_node("number")

func _ready():
	number.set_text("3")

func _on_left_pressed():
	var current = number.get_text()
	var new_number = current
	if current == "3":
		new_number = "1"
	elif current == "5":
		new_number = "3"
	elif current == "7":
		new_number = "5"
	number.set_text(new_number)


func _on_right_pressed():
	var current = number.get_text()
	var new_number = current
	if current == "1":
		new_number = "3"
	elif current == "3":
		new_number = "5"
	elif current == "5":
		new_number = "7"
	number.set_text(new_number)


func get_selected_number():
	return number.get_text()
