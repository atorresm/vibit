# Vibit

Vibit is a simple, 2-player, quick game for Android that proves your reaction speed. Compete with your friends to see who's the fastest!

[See all my games on itch.io](https://papaya-games.itch.io/)

Current version: 1.0

## Download

Vibit is currently available on:
* itch.io: [Game page](https://papaya-games.itch.io/vibit).
* GitLab: [vibit_1.0.apk](/uploads/8e7524d5f860185cfaa81935a42ad897/vibit_1.0.apk). *(You can check all releases [here](https://gitlab.com/atorresm/vibit/tags)).*

## Building from source

### Getting the export template for Android
The export template is needed to enable vibration when exporting to Android. You can get it [here](https://drive.google.com/uc?export=download&id=0BwD3WuJ4P0P8VEF0eWV5aXEybTA). 

*Alternatively, you can build the template yourself. Follow the [official Godot Engine documentation](https://godot.readthedocs.io/en/stable/development/compiling/compiling_for_android.html) and make sure you add the [GodotVibrate module](https://gitlab.com/atorresm/GodotVibrate).*

### Importing and generating the APK from Godot Engine
* Install the [Godot Game Engine](https://godotengine.org/download). This game was developed using the 2.1.3 version.
* Open it and import this project (Clone it and point to it using the **Import** button on the right). Then, open the project.
* Configure Godot Engine to be able to export to Android. Check the [official documentation](https://godot.readthedocs.io/en/stable/learning/workflow/export/exporting_for_android.html#doc-exporting-for-android).
* Click on **Export**, select the Android tab and check the following:
	- Add the export template to the *Release* field in *Custom package*.
	- Fill the *Keystore* fields (*Release, Release User and Release password*).
	- Click on **Export** and select where you want the APK to be saved.

## License

>This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See LICENSE file for the full text of the license.

